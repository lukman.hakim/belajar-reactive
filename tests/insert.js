import {check} from "k6";
import http from "k6/http";

export let options = {
    vus: 1500,
    iterations: 8000
}

export default function () {
    let body;
    let params = {
        headers: {
            "Content-Type": "application/json",
        }
    };

    body = JSON.stringify({
        name: "U" + __VU + " I" + __ITER,
        phone: "0898989900" + __VU + __ITER,
    });

    let save_response = http.post(
        "http://reactive-apps.herokuapp.com/contacts",
        body,
        params
    );

    check(save_response, {
        "save http status 200": (r) => r.status === 200,
        "save respose status 200": (r) => r.json()["status"] === 200
    });

}
