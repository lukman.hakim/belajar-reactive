package com.lukmanh.belajar.reactive.web;

import com.lukmanh.belajar.reactive.entity.Contact;
import com.lukmanh.belajar.reactive.model.BaseResponse;
import com.lukmanh.belajar.reactive.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "contacts")
@RequiredArgsConstructor
public class ContactController {

    private final ContactService service;

    @GetMapping(produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Mono<BaseResponse<List<Contact>>> list() {
        return service.list().collectList().map(BaseResponse::ok);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<BaseResponse<Contact>> save(@RequestBody @Valid Contact contact) {
        return service.save(contact).map(BaseResponse::ok);
    }

}