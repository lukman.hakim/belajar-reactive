package com.lukmanh.belajar.reactive.service;

import com.lukmanh.belajar.reactive.entity.Contact;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ContactService {
    Mono<Contact> save(Contact contact);
    Flux<Contact> list();
}