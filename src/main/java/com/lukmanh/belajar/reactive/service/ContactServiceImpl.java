package com.lukmanh.belajar.reactive.service;

import com.lukmanh.belajar.reactive.entity.Contact;
import com.lukmanh.belajar.reactive.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ContactServiceImpl implements ContactService {

    private final ContactRepository repository;

    @Override
    public Mono<Contact> save(Contact contact) {
        return repository.save(contact);
    }

    @Override
    public Flux<Contact> list() {
        return repository.findAll();
    }
}
