package com.lukmanh.belajar.reactive.repository;

import com.lukmanh.belajar.reactive.entity.Contact;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface ContactRepository extends ReactiveCrudRepository<Contact, String> {
}