package com.lukmanh.belajar.reactive.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class ValidationErrorResponse implements Serializable {
    private static final long serialVersionUID = -5156230216877063325L;
    private final String field;
    private final String message;
}