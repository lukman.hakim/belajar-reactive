package com.lukmanh.belajar.reactive.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table
public class Contact implements Serializable {

    private static final long serialVersionUID = -2553760819809251487L;

    @Id
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String phone;

}
